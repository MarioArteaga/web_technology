import dash
#import dash_core_components as dcc
from dash import dcc
#import dash_html_components as html
from dash import html
from dash.dependencies import Input, Output
import plotly.express as px
import sys
import os
import pandas as pd

app = dash.Dash('app_name')

# import data as dataframe
df = pd.read_csv(r'data/immo.csv')

# create app_dash as app
app = dash.Dash(__name__)

# create variable for dropdown as
State    = df['State'].unique()
Year     = df['BuiltYear'].unique()
AreaM2   = df['M2'].unique()
Category = df['Category'].unique()

# create layout
app.layout = html.Div(
    html.Div([
        html.Div(
            className='app-header',
            children=[
                html.Div('Mario Arteaga, FHNW', className='app-header-title')
            ]
        ),
        html.Div([
            html.H1("Property Price"),
            html.H2("This dashboard shows relevant data from 1800 to 2018 in Switzerland")
        ]),
        html.Div([
            dcc.Markdown("""
                ## Chart 1 
                ### Filter by State
                """),
            dcc.Dropdown(id='select_state',
                         options=[{'label': v, 'value': v} for v in State],
                         multi=True,
                         value=['GE']),

            dcc.Graph(id='graph_state')
        ]),
        html.Div([
            dcc.Markdown(""" 
                ## Chart 2
                ### Filter by Category
                """),
            dcc.Dropdown(id='select-category',
                         options=[{'label': v, 'value': v} for v in Category],
                         multi=True,
                         value=['Haus']),

            dcc.Graph(id='graph2')
        ]),
        html.Div([
            dcc.Markdown("""
                ## Chart 3
                ### Filter by Area
                """),
            dcc.Dropdown(id='select-area',
                         options=[{'label': v, 'value': v} for v in AreaM2],
                         multi=True,
                         value=[140]),

            dcc.Graph(id='graph3')
        ])
    ])
)

@app.callback(Output('graph_state', 'figure'),
              [Input('select_state', 'value')])
def make_figure(select_type):
    dff = df[(df['State'].isin(select_type))]

    fig = px.scatter(
        dff,
        x='BuiltYear',
        y='PurchasePrice',
        template='plotly_dark',
        color_discrete_sequence=["yellow"],
        title='State purchase price'
    )

    fig.update_layout(
        title="State purchase price",
        xaxis_title="Year",
        yaxis_title="Price in CHF"
    )

    return fig

@app.callback(Output('graph2', 'figure'),
              [Input('select-category', 'value')])
def make_figure(select_type):
    dff = df[(df['Category'].isin(select_type))]

    fig = px.scatter(
        dff,
        x='BuiltYear',
        y='PurchasePrice',
        template='plotly_dark',
        color_discrete_sequence=["darkcyan"],
        title='Category purchase price'
    )

    fig.update_layout(
        title="Category purchase price",
        xaxis_title="Year",
        yaxis_title="Price in CHF"
    )

    return fig

@app.callback(Output('graph3', 'figure'),
              [Input('select-area', 'value')])
def make_figure(select_type):
    dff = df[(df['M2'].isin(select_type))]

    fig = px.scatter(
        dff,
        x='BuiltYear',
        y='PurchasePrice',
        template='plotly_dark',
        color_discrete_sequence=["purple"],
        title='Area purchase price'
    )

    fig.update_layout(
        title="Area purchase price",
        xaxis_title="Year",
        yaxis_title="Price in CHF"
    )

    return fig

# run the app
if __name__ == '__main__':
    app.run_server(debug=True)
